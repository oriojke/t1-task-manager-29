package ru.t1.didyk.taskmanager.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.command.data.AbstractDataCommand;
import ru.t1.didyk.taskmanager.command.data.DataBackupLoadCommand;
import ru.t1.didyk.taskmanager.command.data.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

    public void init() {
        load();
        start();
    }

    @Override
    @SneakyThrows
    public synchronized void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            save();
        }
    }
}
