package ru.t1.didyk.taskmanager.exception.field;

public final class PasswordEmptyException extends AbstractFieldException {

    public PasswordEmptyException() {
        super("Error! Password is empty.");
    }

}
