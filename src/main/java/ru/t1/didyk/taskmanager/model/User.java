package ru.t1.didyk.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractModel {

    private final static long serialVersionUID = 1;

    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private Role role = Role.USUAL;

    @Nullable
    private Boolean locked = false;

    @NotNull
    public Boolean isLocked() {
        return locked;
    }

}
