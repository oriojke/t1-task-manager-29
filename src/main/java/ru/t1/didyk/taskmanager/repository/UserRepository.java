package ru.t1.didyk.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.IUserRepository;
import ru.t1.didyk.taskmanager.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        return models
                .stream()
                .filter(user -> id.equals(user.getId()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return models
                .stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return models
                .stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Boolean isLoginExists(@NotNull final String login) {
        return models
                .stream()
                .anyMatch(user -> login.equals(user.getLogin()));
    }

    @Nullable
    @Override
    public Boolean isEmailExists(@NotNull final String email) {
        return models
                .stream()
                .anyMatch(user -> email.equals(user.getEmail()));
    }

}