package ru.t1.didyk.taskmanager.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.component.ISaltProvider;

public interface HashUtil {

    @Nullable
    static String salt(@Nullable final String value, @Nullable final String secret, @Nullable final Integer iteration) {
        if (value == null || secret == null || iteration == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++)
            result = md5(secret + result + secret);
        return result;
    }

    @Nullable
    static String salt(@Nullable final ISaltProvider saltProvider, @Nullable final String value){
        if (saltProvider == null) return null;
        @NotNull final String secret = saltProvider.getPasswordSecret();
        @NotNull final Integer iteration = saltProvider.getPasswordIteration();
        return salt(value, secret, iteration);
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            @NotNull java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
